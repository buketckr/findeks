package step;

import base.BaseTest;
import com.thoughtworks.gauge.Step;
import org.junit.Assert;

public class StepImplementation extends BaseTest {

    @Step("TEB anasayfasina git")
    public void anaSayfayaGit() throws InterruptedException {
        getUrl();
        System.out.println("Anasayfa yuklendi");
    }

    @Step("<saniye> saniye bekle")
    public void waitElement(int key) throws InterruptedException {
        Thread.sleep(key * 1000);
        System.out.println(key + " saniye beklendi");
    }

    @Step("<key> butonu uzerinde bekle")
    public void butonBekle(String key) {
            System.out.println("Kredilere geldi");
            hoverElement(key);
    }

    @Step("<key> elementine tikla")
    public void elementeTikla(String key) {
        //findElement(ihtiyackredilertiklama);
        clickElement(key);
        System.out.println("Ihtiyac kredileri tiklandi");
    }

    @Step("<key> elementi var mi")
    public void checkElement(String key) { try {
        findElement(key);
    } catch (Exception e) {
        //Assert.fail(convertTurkishChar("Element bulunamadi."));
        Assert.fail("Element bulunamadi.");
        }
    }

    @Step("Yeni sekme acildi")
    public void yeniSekmeAc() {
        newTab();
        System.out.println("Yeni sekme acildi");
    }
}